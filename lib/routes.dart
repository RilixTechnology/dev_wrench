import 'package:dev_wrench/page/epoch/epoch_page.dart';
import 'package:dev_wrench/page/main_page.dart';
import 'page/uuid/uuid_page.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> routes = {
  MainPage.routeName: (ctx) => const MainPage(),
  UuidPage.routeName: (ctx) => const UuidPage(),
  EpochPage.routeName: (ctx) => const EpochPage(),
};
