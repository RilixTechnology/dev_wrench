import 'package:dev_wrench/page/epoch/epoch_page.dart';
import 'package:dev_wrench/page/uuid/uuid_page.dart';
import 'package:flutter/material.dart';

class MyMenu extends StatefulWidget {
  const MyMenu({super.key});

  @override
  State<MyMenu> createState() => _MyMenuState();
}

class _MyMenuState extends State<MyMenu> {
  static String _currentRouteName = "";

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListView(
        children: [
          ListTile(
            leading: const Icon(Icons.code),
            title: const Text("UUID Generator"),
            onTap: () => _onMenuItemTapped(UuidPage.routeName),
          ),
          ListTile(
            leading: const Icon(Icons.access_time),
            title: const Text("Epoch Converter"),
            onTap: () => _onMenuItemTapped(EpochPage.routeName),
          )
        ],
      ),
    );
  }

  void _onMenuItemTapped(String routeName) {
    debugPrint("routeName: $routeName");
    if (_currentRouteName == routeName) return;
    _currentRouteName = routeName;
    debugPrint("navigate to: $routeName");
    Navigator.pushNamed(context, routeName);
  }
}
