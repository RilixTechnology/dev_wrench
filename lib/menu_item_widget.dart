import 'package:flutter/material.dart';

class MenuItemWidget extends StatelessWidget {
  final String routeName;
  final String title;

  const MenuItemWidget({
    super.key,
    required this.routeName,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, routeName);
      },
      child: ConstrainedBox(
        constraints: const BoxConstraints(
          minWidth: 180,
          minHeight: 80,
          maxHeight: 80,
          maxWidth: 180,
        ),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Icon(Icons.add),
                const SizedBox(width: 5),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(title),
                    const Text("Subtitle"),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
