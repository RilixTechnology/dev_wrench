import 'package:dev_wrench/page/epoch/epoch_page.dart';
import 'package:flutter/material.dart';
import '../menu_item_widget.dart';
import 'uuid/uuid_page.dart';

class MainPage extends StatefulWidget {
  static const routeName = "/";

  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dev Wrench"),
      ),
      body: const Column(
        children: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Wrap(
              alignment: WrapAlignment.center,
              children: [
                MenuItemWidget(
                  routeName: UuidPage.routeName,
                  title: 'UUID Generator',
                ),
                MenuItemWidget(
                  routeName: EpochPage.routeName,
                  title: "Epoch Converter",
                ),
                MenuItemWidget(
                  routeName: "/",
                  title: "Sample",
                ),
                MenuItemWidget(
                  routeName: "/",
                  title: "Sample",
                ),
                MenuItemWidget(
                  routeName: "/",
                  title: "Sample",
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
