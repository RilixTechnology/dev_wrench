import 'package:dev_wrench/page/uuid/uuid_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';

import 'empty_uuid.dart';

class UuidGeneratorWidget extends StatefulWidget {
  final UuidType uuidType;

  const UuidGeneratorWidget({
    super.key,
    required this.uuidType,
  });

  @override
  State<UuidGeneratorWidget> createState() => UuidGeneratorWidgetState();
}

class UuidGeneratorWidgetState extends State<UuidGeneratorWidget> {
  String _result = "";

  @override
  void initState() {
    super.initState();
    refresh();
  }

  @override
  void didUpdateWidget(UuidGeneratorWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.uuidType != oldWidget.uuidType) {
      refresh();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text("Your ${widget.uuidType}:"),
        const SizedBox(height: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SelectableText(
              _result,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(width: 20),
            ElevatedButton.icon(
              onPressed: _copyToClipboard,
              icon: const Icon(Icons.copy),
              label: const Text("Copy"),
            ),
          ],
        ),
        const SizedBox(height: 15),
        TextButton(
          onPressed: refresh,
          child: const Text(
            "Refresh to generate",
            style: TextStyle(
              decoration: TextDecoration.underline,
            ),
          ),
        ),
      ],
    );
  }

  void _copyToClipboard() async {
    await Clipboard.setData(ClipboardData(text: _result));
    var snackBar = const SnackBar(content: Text("Copied to clipboard"));
    if(mounted) ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void refresh() {
    var uuid = const Uuid();
    String result = "";
    switch (widget.uuidType) {
      case UuidType.version1:
        result = uuid.v1();
        break;
      case UuidType.version4:
        result = uuid.v4();
        break;
      case UuidType.nil:
        result = emptyOrNilUUID;
        break;
      case UuidType.version7:
        result = uuid.v7();
        break;
    }
    setState(() => _result = result);
  }
}
