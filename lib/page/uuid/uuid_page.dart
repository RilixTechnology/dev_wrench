import 'package:dev_wrench/page/uuid/uuid_generator_bulk_widget.dart';
import 'package:dev_wrench/page/uuid/uuid_generator_widget.dart';
import 'package:dev_wrench/page/uuid/uuid_type.dart';
import 'package:dev_wrench/page_body.dart';
import 'package:dev_wrench/radio_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class UuidPage extends StatefulWidget {
  static const routeName = "/uuid";

  const UuidPage({super.key});

  @override
  State<UuidPage> createState() => _UuidPageState();
}

class _UuidPageState extends State<UuidPage> {
  UuidType _selectedItem = UuidType.version4;

  final FocusNode _focusNode = FocusNode();
  final _uuidGenKey = GlobalKey<UuidGeneratorWidgetState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("UUID Generator"),
        centerTitle: true,
      ),
      body: PageBody(
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: KeyboardListener(
            focusNode: _focusNode,
            autofocus: true,
            onKeyEvent: _handleKeyEvent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  alignment: WrapAlignment.center,
                  spacing: 15,
                  children: [
                    RadioButton<UuidType>(
                      value: UuidType.version1,
                      groupValue: _selectedItem,
                      onChanged: _onUuidRadioChanged,
                      title: "Version 1 UUID",
                    ),
                    RadioButton<UuidType>(
                      value: UuidType.version4,
                      groupValue: _selectedItem,
                      onChanged: _onUuidRadioChanged,
                      title: "Version 4 UUID",
                    ),
                    RadioButton<UuidType>(
                      value: UuidType.nil,
                      groupValue: _selectedItem,
                      onChanged: _onUuidRadioChanged,
                      title: "Empty/Nil UUID",
                    ),
                    RadioButton<UuidType>(
                      value: UuidType.version7,
                      groupValue: _selectedItem,
                      onChanged: _onUuidRadioChanged,
                      title: "Version 7 UUID",
                    ),
                  ],
                ),
                const SizedBox(height: 25),
                UuidGeneratorWidget(
                  key: _uuidGenKey,
                  uuidType: _selectedItem,
                ),
                const Divider(thickness: 0.4),
                const SizedBox(height: 20),
                Flexible(
                    child: UuidGeneratorBulkWidget(uuidType: _selectedItem)),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onUuidRadioChanged(UuidType? uuidType) {
    setState(() => _selectedItem = uuidType ?? UuidType.version4);
  }

  KeyEventResult _handleKeyEvent(KeyEvent event) {
    if (event is KeyDownEvent && event.logicalKey == LogicalKeyboardKey.f5) {
      _uuidGenKey.currentState?.refresh();
    }
    return event.logicalKey == LogicalKeyboardKey.f5
        ? KeyEventResult.handled
        : KeyEventResult.ignored;
  }
}
