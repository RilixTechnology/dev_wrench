import 'dart:io';

import 'package:dev_wrench/numerical_range_formatter.dart';
import 'package:dev_wrench/page/uuid/uuid_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';

import 'empty_uuid.dart';

class UuidGeneratorBulkWidget extends StatefulWidget {
  final UuidType uuidType;

  const UuidGeneratorBulkWidget({super.key, required this.uuidType});

  @override
  State<UuidGeneratorBulkWidget> createState() =>
      _UuidGeneratorBulkWidgetState();
}

class _UuidGeneratorBulkWidgetState extends State<UuidGeneratorBulkWidget> {
  final TextEditingController _numberController = TextEditingController();

  List<String> _items = [];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text("Bulk ${widget.uuidType} Generation"),
        const SizedBox(height: 15),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            const Text("How Many? "),
            Container(
              decoration: BoxDecoration(
                border: Border.all(width: 1),
                borderRadius: const BorderRadius.all(
                  Radius.circular(5.0),
                ),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: SizedBox(
                      width: 150,
                      child: TextField(
                        controller: _numberController,
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                          const NumericalRangeFormatter(min: 1, max: 500),
                        ],
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                          hintText: "Max 500",
                          isDense: true,
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                        ),
                      ),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: _generate,
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("Generate"),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(width: 15),
            ElevatedButton.icon(
              onPressed: _copyToClipboard,
              icon: const Icon(Icons.copy),
              label: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text("Copy"),
              ),
            ),
          ],
        ),
        Expanded(
          child: SelectionArea(
            child: Center(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: _items.length,
                itemBuilder: (ctx, idx) {
                  return Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SelectionContainer.disabled(
                        child: SizedBox(
                          width: 60,
                          child: Text(
                            "${idx + 1}.",
                            style: const TextStyle(fontWeight: FontWeight.w300),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.5),
                        child: Text(_items[idx]),
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _generate() {
    var items = _generateItems();
    setState(() => _items = items);
  }

  List<String> _generateItems() {
    var uuid = const Uuid();
    var total = int.tryParse(_numberController.text) ?? 1;
    List<String> items = [];
    for (int i = 0; i < total; i++) {
      String item = "";
      switch (widget.uuidType) {
        case UuidType.version1:
          item = uuid.v1();
          break;
        case UuidType.version4:
          item = uuid.v4();
          break;
        case UuidType.version7:
          item = uuid.v7();
          break;
        case UuidType.nil:
          item = emptyOrNilUUID;
          break;
      }
      items.add(item);
    }
    return items;
  }

  void _copyToClipboard() async {
   var items = _generateItems();
    var result = items.join(Platform.lineTerminator);
    await Clipboard.setData(ClipboardData(text: result));
    var snackBar = const SnackBar(content: Text("Copied to clipboard"));
    if (mounted) ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
