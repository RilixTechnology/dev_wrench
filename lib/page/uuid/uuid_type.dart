enum UuidType {
  version1, // version 1 UUID
  version4, // version 4 UUID
  version7, // Version 7 UUID
  nil, // Nil/Empty UUID
}
