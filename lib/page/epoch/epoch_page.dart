import 'package:dev_wrench/page/epoch/epoch_clock_widget.dart';
import 'package:dev_wrench/page_body.dart';
import 'package:flutter/material.dart';

class EpochPage extends StatefulWidget {
  static const routeName = "/epoch";

  const EpochPage({super.key});

  @override
  State<EpochPage> createState() => _EpochPageState();
}

class _EpochPageState extends State<EpochPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Epoch Converter"),
      ),
      body: const PageBody(
        body: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(child: EpochClockWidget()),
            ],
          ),
        ),
      ),
    );
  }
}
