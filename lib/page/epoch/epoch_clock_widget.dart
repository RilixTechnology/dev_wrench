import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EpochClockWidget extends StatefulWidget {
  const EpochClockWidget({super.key});

  @override
  State<EpochClockWidget> createState() => _EpochClockWidgetState();
}

class _EpochClockWidgetState extends State<EpochClockWidget> {
  final DateFormat _dateFormat = DateFormat("EEEE MMMM dd yyyy HH:mm:ss");
  String _currentDateTimeInUTC = "";

  @override
  Widget build(BuildContext context) {
    return SelectionArea(
      child: StreamBuilder(
          stream: Stream.periodic(const Duration(seconds: 1)),
          builder: (ctx, snapshot) {
            var now = DateTime.timestamp();
            var data = _getTimeInUTC(now);
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  data.$1,
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge
                      ?.copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 15),
                Text(
                  data.$2.toString(),
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                const SizedBox(height: 5),
                const Text("Seconds Since Jan 01 1970. (UTC)"),
                const SizedBox(height: 10),
                Text(
                  "HEX: ${data.$2.toRadixString(16).toUpperCase()}",
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 20),
                Text("In Milliseconds: ${data.$3.toString()}"),
              ],
            );
          }),
    );
  }

  (String timeInUtc, int secondSinceEpoch, int milliseconds) _getTimeInUTC(
      DateTime dateTimeInUtc) {
    var val = _dateFormat.format(dateTimeInUtc);
    int millis = dateTimeInUtc.millisecondsSinceEpoch;
    int secondSinceEpoch = millis ~/ 1000;
    return ("$val UTC".toUpperCase(), secondSinceEpoch, millis);
  }
}
