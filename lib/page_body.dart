import 'package:dev_wrench/my_menu.dart';
import 'package:flutter/material.dart';

class PageBody extends StatelessWidget {
  final Widget body;

  const PageBody({super.key, required this.body});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Flexible(
          flex: 2,
          child: MyMenu(),
        ),
        Flexible(
          flex: 8,
          child: body,
        ),
      ],
    );
  }
}
